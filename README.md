# Buka laman berikut di web-browser

https://gitlab.com/sdmoko/project01


# Pasang Git di laptop

- Install Git jika OS Laptop Windows
https://gitforwindows.org/

- Install Git jika OS Laptop MacOS
https://git-scm.com/download/mac

- Install Git jika OS Laptop Linux
https://book.git-scm.com/2_installing_git.html

# Membuat project (repository) baru di gitlab

1. Buat akun di gitlab.com

- Login di Gitlab

- buka aplikasi git-bash di windows
- jika MacOS bisa pakai terminal jika sudah terpasang git

```
git config --global user.name "Nama Saya"
git config --global user.email "emailsaya@example.com"
git config --list
```

2. Buat Repo

Akses Gitlab Via Browser

Create Project Baru

Nama project: project01

Visibility Level : Public

3. Clone repo

Ganti USERNAME dengan username gitlab masing-masing
```
git clone https://gitlab.com/USERNAME/project01.git
cd project01
```

buat file dengan nama README.md di notepad dengan isi:

```

# Meetup SKK Technical: 

- Hosted by PT SKK
- Speakers: Moko

## Agenda

Full Agenda:

- 08:00 - 09:30 Git Introduction

```
Simpan berkas dan jalankan
```
git status
```

4. Menambahkan file ke staging area
```
git add README.md
git commit -m "add README.md"
git status
```

5. Sinkronisasi perubahan ke remote repository
```
git push origin master
git status
```

6. Melihat perbedaan file di working directory dengan staging area

Ubah berkas README.md menjadi berikut

```

# Meetup SKK Technical: 

- Hosted by PT SKK
- Speakers: Moko, Ahmad Haris 

## Agenda

Full Agenda:

- 08:00 - 09:30 Git Introduction
- 09:30 - 10.00 GitLab Project Management
- 10.00 - 10.30 Coffee Break

```

```
git status
git diff README.md
```

7. Sinkronisasi dan melihat commit di GitLab

```
git push origin master
```
Buka web https://gitlab.com/USERNAME/project01

Lihat bagian `repositories`

8. Membuat berkas dan direktori baru

- Buat direktori baru dengan nama `slide`
- Buat berkas txt dengan nama `slide.txt` di direktori/foler `slide`
- Tambahkan direktori dan berkas baru tersebut di git

```
mkdir slide
cd slide
touch slide/slide.txt
cd ..
git status
git add slide
git add slide/slide.txt
git status
git commit -m "Add slide.txt"
```

9. Menghapus berkas dari working dir dan staging area

```
git rm slide/slide.txt
git status
git commit -m "delete slide.txt"
git push origin master
```

10. Cek di web gitlab, repo project01 apakah filenya masih ada atau belum

11. Periksa Log apakah ada commit dengan user dan email kamu

```
git log
```

12. Buat Branch baru

```
git branch
git branch develop
git branch
```

13. Pindah Branch atau cabang

```
git checkout develop
git branch
```

Perhatikan saat ini sudah di branch `develop`

14. Buat berkas baru di branch `develop`

Buat berkas `develop.txt` di rootdir repositori dan isi dengan:

```
Ini isi berkas develop di branch develop
```

Lalu tambahkan, commit dan push
```
git add develop.txt
git commit -m "add develop.txt"
git push origin develop
```

Perhatikan di web Gitlab perbedaan branch `master` dan `develop`

15. Merge request branch `develop` ke branch `master`

Buka dari web dan repo gitlab project01

Masuk menu Merge Request > New Merge Request

Pilih Source Branch `Develop`

Pilih Target Branch `Master`

Klik `Compare branches and Continue`

Lalu sebagai verifikator lihat diff merge requet, jika sesuai klik Merge untuk menggabungkan branch

Lihat hasilnya di `repositories`

16. Bekerja kolaborasi dengan Groups

Buka Groups
https://gitlab.com/cooptech

Jika belum bisa, silahkan infokan username gitlab untuk ditambahkan sebagai member

17. Clone Repo dari Groups COOPTECH

Pastikan keluar dari direktori kerja git project01
```
git clone https://gitlab.com/cooptech/kolaborasi.git
cd kolaborasi
```

18. Buat branch baru

Ganti USERNAME dengan username gitlab kamu
```
git checkout -b USERNAME
```

19. Buat file baru dengan nama USERNAME.txt

Isi berkas dengan
```
Nama Lengkap: Nama Lengkap Anda
CU: Nama CU
```

Lalu add, commit dan push
```
git add USERNAME.txt
git commit -m "add USERNAME.txt"
git push origin USERNAME
git status
```

20. Buat Merge Request branch `USERNAME` ke branch `MASTER`

Buka dari web dan repo gitlab project01

Masuk menu Merge Request > New Merge Request

Pilih Source Branch `USERNAME`

Pilih Target Branch `Master`

Klik `Compare branches and Continue`

Biarkan penyaji untuk mengecek merge request

